<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['middleware' => 'auth'], function () {
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/customer',[App\Http\Controllers\CustomerController::class,'index']);
Route::get('/create',[App\Http\Controllers\CustomerController::class,'create']);
Route::get('/store',[App\Http\Controllers\CustomerController::class,'store']);
Route::get('/edit/{id}',[App\Http\Controllers\CustomerController::class,'edit']);
Route::get('/show/{id}',[App\Http\Controllers\CustomerController::class,'show']);
Route::get('/update/{id}',[App\Http\Controllers\CustomerController::class,'update']);
Route::get('/destroy/{id}',[App\Http\Controllers\CustomerController::class,'destroy']);
Route::get('/customer/get_ajax_data',[App\Http\Controllers\CustomerController::class, 'get_ajax_data']);
Route::get('/customer/datarange',[App\Http\Controllers\CustomerController::class,'daterange'])->name('customer.datarange');
});
