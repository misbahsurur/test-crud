<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Http\Requests\StoreCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Customer::orderBy('nama', 'ASC')->paginate(10);
        return view('index',compact('data'));
    }

    // public function read()
    // {
    //     $data = Customer::orderBy('nama', 'ASC')->paginate(10);
    //     return view('read')->with([
    //         'data' => $data
    //     ]);
    // }

    function get_ajax_data(Request $request)
    {
        if($request->ajax())
        {
            if($request->from_date != '' && $request->to_date != '')
            {
                $data = Customer::whereBetween('date', array($request->from_date, $request->to_date))->orderBy('tgl_reg', 'ASC')
                ->paginate(10);
            } else {
                $query = $request->get('query');
                $query = str_replace(" ", "%", $query);
                $data = Customer::where('nama', 'like', '%'.$query.'%')
                        ->orWhere('alamat', 'like', '%'.$query.'%')
                        ->orWhere('no_telp', 'like', '%'.$query.'%')
                        ->orderBy('nama', 'ASC')
                        ->paginate(10);
            }
            return view('pagination_data', compact('data'))->render();
        }
    }

    function daterange(Request $request)
    {
        if($request->from_date != '' && $request->to_date != '')
            {
                $data = Customer::whereBetween('tgl_reg', array($request->from_date, $request->to_date))->orderBy('tgl_reg', 'ASC')
                ->paginate(10);
            } else {
                $query = $request->get('query');
                $query = str_replace(" ", "%", $query);
                $data = Customer::where('nama', 'like', '%'.$query.'%')
                        ->orWhere('alamat', 'like', '%'.$query.'%')
                        ->orWhere('no_telp', 'like', '%'.$query.'%')
                        ->orderBy('nama', 'ASC')
                        ->paginate(10);
            }
            return view('index', compact('data'))->render();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCustomerRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Customer::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Customer::findOrFail($id);
        return view('detail')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Customer::findOrFail($id);
        return view('edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCustomerRequest  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $data = Customer::findOrFail($id);
        $data->nama = $request->nama;
        $data->alamat = $request->alamat;
        $data->no_telp = $request->no_telp;
        $data->tgl_reg = $request->tgl_reg;
        $data->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Customer::findOrFail($id);
        $data->delete();

    	return 'sukses';
    }
}
