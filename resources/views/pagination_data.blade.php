<div class="row">
	<div class="col-lg-12">

		<table class="table table-bordered" id="laravel">
		   <thead>
			  <tr>
                <th>No</th>
                <th class="sorting" data-sorting_type="asc" data-column_name="nama" style="cursor: pointer">Nama<span id="nama_icon"></th>
                <th>Alamat</th>
                <th>No Telepon</th>
                <th>Aksi</th>
			  </tr>
		   </thead>
		   <tbody>
				@if(!empty($data) && $data->count())
                @foreach ($data as $item)
				  <tr>
                    <td>{{ ($data->currentPage() - 1)  * $data->links()->paginator->perPage() + $loop->iteration }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->alamat }}</td>
                    <td>{{ $item->no_telp }}</td>
                    <td>
                        <button class="btn btn-warning" onClick="edit({{ $item->id }})">Edit</button>
                        <button class="btn btn-danger" onClick="destroy({{ $item->id }})">Delete</button>
                        <button class="btn btn-info" onClick="show({{ $item->id }})">Detail</button>
                    </td>
				  </tr>
				  @endforeach
				@else
				<tr>
					<td colspan="4">No data found.</td>
				</tr>
				@endif
		   </tbody>
		</table>
        <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
        <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="id" />
        <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc" />
	</div>
</div>
<div id="pagination">
    {{ $data->links() }}
</div>
