
<table class="table">
    <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Alamat</th>
        <th>No Telepon</th>
        <th>Aksi</th>
    </tr>
    @foreach ($data as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->nama }}</td>
            <td>{{ $item->alamat }}</td>
            <td>{{ $item->no_telp }}</td>
            <td>
                <button class="btn btn-warning" onClick="edit({{ $item->id }})">Edit</button>
                <button class="btn btn-danger" onClick="destroy({{ $item->id }})">Delete</button>
                <button class="btn btn-info" onClick="show({{ $item->id }})">Detail</button>
            </td>
        </tr>
    @endforeach
</table>
