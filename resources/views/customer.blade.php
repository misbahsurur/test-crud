@extends('layouts.main')
@section('title', 'Halaman Customer')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Tabel Customer</div>

                <div class="card-body">
                    <div class="table-responsive">
                        <a class="btn btn-primary" href="javascript:;" data-bs-toggle="modal" data-bs-target="#formCustomer">
                            Tambah Data
                        </a>
                        <p></p>
                        <table class="table table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="text-center">Nama</th>
                                    <th class="text-center">Alamat</th>
                                    <th class="text-center">No Telepon</th>
                                    <th class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div id="formCustomer" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Form Customer</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="form-group col-xs-12 col-lg-12">
                    <label class="control-label">Nama</label>
                    {{ Form::text('nama', null, ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-lg-12">
                    <label class="control-label">Alamat</label>
                    {{ Form::text('alamat', null, ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-lg-12">
                    <label class="control-label">No Telepon</label>
                    {{ Form::number('no_telp', null, ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-xs-12 col-lg-12">
                    <label class="control-label">Tanggal Registrasi</label>
                    {{ Form::date('tgl_reg', null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="submit()">Save</button>
                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
