<div class="p2">
    <div class="form-group">
        <label>Nama</label>
        <input type="text" name="nama" id="nama" class="form-control" placeholder="Masukkan Nama">
        <label>Alamat</label>
        <input type="text" name="alamat" id="alamat" class="form-control" placeholder="Masukkan Alamat">
        <label>No Telepon</label>
        <input type="number" name="no_telp" id="no_telp" class="form-control" placeholder="Masukkan Nomor Telepon">
        <label>Taggal Registrasi</label>
        <input type="date" name="tgl_reg" id="tgl_reg" class="form-control">
    </div>
    <div class="form-group mt-2">
        <button class="btn btn-success" onClick="store()">Tambah</button>
    </div>
</div>
