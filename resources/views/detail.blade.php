<div class="p2">
    <div class="form-group">
        <label>Nama</label>
        <input type="text" name="nama" id="nama" class="form-control" value="{{$data->nama}}" readonly>
        <label>Alamat</label>
        <input type="text" name="alamat" id="alamat" class="form-control" value="{{$data->alamat}}"  readonly>
        <label>No Telepon</label>
        <input type="text" name="no_telp" id="no_telp" class="form-control" value="{{$data->no_telp}}"  readonly>
        <label>Taggal Registrasi</label>
        <input type="date" name="tgl_reg" id="tgl_reg" value="{{$data->tgl_reg}}"  class="form-control" readonly>
    </div>
</div>
